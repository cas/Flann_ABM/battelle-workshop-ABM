import pandas as pd
import matplotlib.pyplot as plt 
import ast

people = pd.read_csv('People0.txt', sep = ';', header=None)
people.columns=['name', 'type', 'data']
people2 = pd.read_csv('People1.txt', sep=';', header=None)
people2.columns=['name', 'type', 'data']

df = pd.concat([people, people2]).reset_index(drop=True)

# Shortest distance journey.
y2 = df.loc[1, 'data']
y2 = ast.literal_eval(y2)
y2 = [30-y for y in y2]

x2 = [i for i in range(len(y2))]

# Journey with stroll - take detour if more enjoyable.
y1 = df.loc[0, 'data']
y1 = ast.literal_eval(y1)
y1 = [30-y for y in y1]

x1 = [i/len(y1)*len(y2) for i in range(len(y1))]

# Make shortest journey appear artificially shorter.

x1 = [i * 1.25 for i in x1]

# plt.plot(x1,y1)
# plt.title('Walking experience score with respect to time.')
# plt.xlabel('Cycle number')
# plt.ylabel('Walking experience score')
# plt.show()



plt.plot(x1,y1, color = 'orange', label = 'Stroll')
plt.plot(x2,y2, color = 'blue', label = 'Shortest path')
plt.title('Walking experience score per journey segment.')
plt.xlabel('Journey progression')
plt.ylabel('Walking experience score')
plt.legend()
plt.show()


