model walkmodel

global {
	string RoadsPath <- '../includes/shp2/Roads.shp';
	string BuildingsPath <- '../includes/shp2/Buildings.shp';
	string TreesPath <- '../includes/shp2/Trees.shp';
	string BoundsPath <- '../includes/shp/Bounds.shp';
	file RoadsSHP <- file(RoadsPath);
	file BuildingsSHP <- file(BuildingsPath);
	geometry shape <- envelope(BuildingsSHP);
	graph roadsGraph;
	map<Roads,float> weightedRoads;
	graph weightedRoadsGraph;
	
	int maxCount;
	
	int treeDistance <- 15;
	
	int nPeople <- 2;
	
	int iPeople <- 0;
	
	Buildings origin;
	Buildings globalDest;
	
	
	init {
		create Trees from: file(TreesPath) with: [diam1m::int(read('D_1M'))];
		ask Trees {
			do extrapolateDiam;
		}
		
		create Buildings from: file(BuildingsPath) with: [type::string(read ("NOMEN_CLAS"))] {
			if type="Habitation" {
				color <- #pink ;
			}
		}
		
		point pA <- {990.0155818181282, 42.69734545483846};
		point pB <- {461.6710666662289, 193.01908888926522};
		origin <- agent_closest_to(pA);
		globalDest <- agent_closest_to(pB);
		
		
		create Roads from: RoadsSHP;
		ask Roads {
			do countTrees;
		}
		maxCount <- Roads max_of (each.nTrees);
		ask Roads {
			do computeScore;
		}
		roadsGraph <- as_edge_graph(Roads);
		weightedRoads <- Roads as_map (each:: (each.score * each.shape.perimeter));
		weightedRoadsGraph <- as_edge_graph(Roads) with_weights weightedRoads;
		
		
		create People number: nPeople {
			//home <- one_of(Buildings);
			home <- origin;
			location <- home.location;
			speed <- 1.11;
			theBuilding <- one_of(Buildings);
			if (iPeople mod 2 = 0) {
				type <- 'nicest';
				color <- #yellow;
			} else {
				type <- 'shortest';
				color <- #red;
			}
			iPeople <- iPeople + 1;
		}
		
		
		
		
		
				
	}
	
	
	reflex newDay when: People all_verify (each.endedJourney) {
		write 'Start of new day.';
		ask People {
			endedJourney <- false;
		}
	}
	
}


species Buildings {
	string type; 
	rgb color <- #gray  ;
	
	aspect base {
		draw shape color: color ;
	}
}

species Roads  {
	rgb defaultColor <- #black ;
	rgb scaledColor;
	float score;
	int nTrees;
	float red;
	
	action countTrees {
		nTrees <- length(Trees at_distance treeDistance);
	}
	
	action computeScore {
		score <- 2 - 1 * (nTrees/maxCount)^(1/1000);
	}
	
	aspect base {
		draw shape color: defaultColor ;
	}
	
	aspect colorScale {
		red <- 0 + 255 * sqrt(nTrees/maxCount);
		draw shape color: rgb(red,0,0);
	}
}

species Trees {
	rgb color <- rgb(0, 97, 24);
	float diam1m;
	
	action extrapolateDiam {
		if diam1m = 0 {
			diam1m <- (Trees where (each.diam1m != 0)) mean_of (each.diam1m);
		}
	}
	
	aspect base {
		draw circle(diam1m/100*5) color: color;
	}
}

species People skills:[moving] {
	rgb color <- #yellow ;
	Buildings home;
	Buildings dest;
	string objective;
	point theTarget;
	bool endedJourney <- false;
	bool returnTrip <- false;
	Buildings theBuilding;
	list scores;
	string type;
	
		
	action pickDest {
		dest <- globalDest;
		//dest <- theBuilding;
		//dest <- one_of(Buildings);
		theTarget <- dest.location;	
		returnTrip <- false;	
	}
	
	action walk {
		do goto target: theTarget on: roadsGraph;
	}
	
	action stroll {
		do goto target: theTarget on: weightedRoadsGraph;
	}
	
	action saveOutput {
		save name + ';' + scores to: '../output/' + name + '.txt' format: text header: false rewrite: false;
	}
	
	
	reflex general {
		if dest = nil {
			if not(endedJourney) {
				do pickDest;
			}
			
		}
		else {
			if location distance_to theTarget < 5 {
				location <- theTarget.location;
				if theTarget = dest.location {
					theTarget <- home.location;
					returnTrip <- true;
				} else if theTarget = home.location {
					write name + ' ended journey';
					endedJourney <- true;
					dest <- nil;
					do saveOutput;
					scores <- [];
				}
				
			} 
			else {
				
				if type = 'shortest' {
					do walk;
				}
				else if type = 'nicest' {
					do stroll;
				}
				
			}
			
		}
	} 
	
	reflex recordScore when: not(endedJourney) {
		if current_edge != nil {
			add Roads(current_edge).score to: scores;
		}
		
	}
	 
	
	aspect base {
		draw circle(10) color: color border: #black;
	}
}

experiment Walk type: gui {
	output {
		display city_display type: java2D {
			species Buildings aspect: base ;
			species Roads aspect: base ;
			species Trees aspect: base;
			species People aspect: base ;
		}
		
		display trees_roads type:java2D {
			species Buildings aspect: base;
			species Roads aspect: colorScale;
			species Trees aspect: base;
		}
	}
}
