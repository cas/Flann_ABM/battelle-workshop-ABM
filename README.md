## Welcome to the repo for the Workshop at CUI-Battelle, Geneva, on digital innovation for urban planning.
Dates: November 6, 2023 to November 10, 2023.

When opening GAMA, set the workspace to the folder containing this readme.
The model file, input and output files are contained in the **'WalkProject'** folder.